rit.cen.awx-docker
==================

Ensure that an Ansible AWX docker container is running and is responding to HTTP requests.

**NOTE**: This role is based on the official AWX install method, but with openshift and local build components removed. Also, the docker-compose option has been removed.

Requirements
------------

The following requirements **must** be met before using this role:

- The Docker engine must be installed and running on the target host

- The docker-py python module must be installed on the target host

- Network connection to https://hub.docker.com must be available from the target host

Role Variables
--------------

Since this role is based on the official Ansible AWX installer, it uses the same set of variables as the AWX installer.

The following variables **must** be defined either as role variables or as host/group variables:


dockerhub_base=ansible
dockerhub_version=latest
rabbitmq_version=3.6.14

awx_secret_key=awxsecret

postgres_data_dir=/tmp/pgdocker
host_port=80

pg_username=awx
pg_password=awxpass
pg_database=awx
pg_port=5432

docker_network: The name of an existing Docker User network that all the AWX related containers will get connected to


Dependencies
------------

This role does not pull in any automatic dependencies.

Example Playbook
----------------

Including an example of how to use your role (for instance, with variables passed in as parameters) is always nice for users too:

    - hosts: centos
      vars:
        dockerhub_base: ansible
        dockerhub_version: latest
        rabbitmq_version: 3.6.14
        awx_secret_key: S3cr3t
        postgres_data_dir: /srv/docker/awx_postgres
        host_port: 8080
        pg_username: awx
        pg_password: awxsecretpass
        pg_database: awx
        pg_port: 5432
        docker_network: awxnetwork
      roles:
         - { role: rit.cen.awx-docker }

License
-------

BSD

Author Information
------------------

Contact http://www.regal-it.com.au/
